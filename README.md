The `mod_case` module for ProFTPD provides case-insensitivity support
to FTP commands; useful for dealing with Windows FTP clients.

For further module documentation, see the [mod_case.html](https://htmlpreview.github.io/?https://github.com/Castaglia/proftpd-mod_case/blob/master/mod_case.html) documentation.
